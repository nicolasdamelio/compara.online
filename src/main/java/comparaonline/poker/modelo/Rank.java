package comparaonline.poker.modelo;

/**
 * Rank
 * Ejemplo: Rank(int 11, rank "J"). 
 * 
 * @author Nicol�s D'Amelio
 * @version 1.0
 */

public class Rank {
	
	private int value;
	private String rank;
	
	public Rank(int value, String rank) {
		super();
		this.value = value;
		this.rank = rank;
	}
	
	public int getValue() {
		return value;
	}
	
	public void setValue(int value) {
		this.value = value;
	}
	
	public String getRank() {
		return rank;
	}
	
	public void setRank(String rank) {
		this.rank = rank;
	}
	
	@Override
	public String toString() {
		return rank;
	}
	
}
