package comparaonline.poker.modelo;


/**
 * Card
 * Ejemplo: Rank(int 11, rank "J"), suit "spades". 
 * 
 * @author Nicol�s D'Amelio
 * @version 1.0
 */
public class Card implements Comparable<Card> {
	
	private Rank rank;
	private String suit;
	
	public Card(Rank rank, String suit) {
		super();
		this.rank = rank;
		this.suit = suit;
	}

	public Rank getRank() {
		return rank;
	}

	public void setRank(Rank rank) {
		this.rank = rank;
	}

	public String getSuit() {
		return suit;
	}

	public void setSuit(String suit) {
		this.suit = suit;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Card [rank=");
		builder.append(rank);
		builder.append(", suit=");
		builder.append(suit);
		builder.append("]");
		return builder.toString();
	}
	
	@Override
	//Metodo para ordenar las cartas de menor a mayor.
	public int compareTo(Card compareCard) {
		int compareRank = ((Card) compareCard).getRank().getValue();

		//ascending order
		return this.rank.getValue() - compareRank;
	}
}
