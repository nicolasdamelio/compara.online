package comparaonline.poker.modelo;

import java.util.Arrays;

/**
 * Hand
 * Clase principal donde se asigna la mano y esta la logica para saber que mano se tiene.
 * 
 * @author Nicol�s D'Amelio
 * @version 1.0
 */
public class Hand {
	
	private Card[] hand = new Card[5];
	private Card highFourKind;
	private static boolean isFlush = false;
	private static boolean isStraight = false;
	private static boolean isThreeKind = false;
	private Card highThreeKind;
	private static boolean isOnePair = false;
	private Card highOnePair;
	private Card highTwoPair1;
	private Card highTwoPair2;
	
	public Card getHighFourKind() {
		return highFourKind;
	}

	public Card getHighThreeKind() {
		return highThreeKind;
	}

	public Card getHighOnePair() {
		return highOnePair;
	}

	public Card getHighTwoPair1() {
		return highTwoPair1;
	}

	public Card getHighTwoPair2() {
		return highTwoPair2;
	}

	public enum HandRank {
	    Royal_Flush(10),
	    Straight_Flush(9),
	    Four_of_a_Kind(8),
	    Full_House(7),
	    Flush(6),
	    Straight(5),
	    Three_of_a_Kind(4),
	    Two_Pair(3),
	    One_Pair(2),
	    High_Card(1);
	    
	    private int points;

	    HandRank(int points) {
	        this.points = points;
	    }

	    public int points() {
	        return points;
	    }
	}

	public Hand(Card[] hand) {
	    this.hand = hand;
	}

	public Card[] getHand() {
	    return hand;
	}

	public void setHand(Card[] hand) {
	    this.hand = hand;
	}

	
	public HandRank determineHandRank() {
		//Ordeno las cartas de la mano de menor a mayor
		Arrays.sort(hand);
	    if (isRoyalFlush()) {
	        return HandRank.Royal_Flush;
	    } else if (isStraightFlush()) {
	        return HandRank.Straight_Flush;
	    } else if (isFourOfAKind()) {
	        return HandRank.Four_of_a_Kind;
	    } else if (isFullHouse()) {
	        return HandRank.Full_House;
	    } else if (isFlush) {
	        return HandRank.Flush;
	    } else if (isStraight) {
	        return HandRank.Straight;
	    } else if (isThreeKind) {
	        return HandRank.Three_of_a_Kind;
	    } else if (isTwoPair()) {
	        return HandRank.Two_Pair;
	    } else if (isOnePair) {
	        return HandRank.One_Pair;
	    } else {
	        return HandRank.High_Card;
	    }

	}

	public boolean isRoyalFlush() {
		//Si es Straight y Flush, verifico que exista "10, J, Q, K y A".
		isStraight = isStraight();
		isFlush = isFlush();
	    if (isStraight && isFlush) {
	        boolean aceExists = false, kingExists = false, queenExists = false, jackExists = false, tenExists = false;
	        for (Card c : hand) {
	            switch (c.getRank().toString()) {
	                case "A":
	                    aceExists = true;
	                    break;
	                case "K":
	                    kingExists = true;
	                    break;
	                case "Q":
	                    queenExists = true;
	                    break;
	                case "J":
	                    jackExists = true;
	                    break;
	                case "10":
	                    tenExists = true;
	                    break;

	            }
	        }
	        return (aceExists && kingExists && queenExists && jackExists && tenExists);
	    } else {
	        return false;
	    }
	}

	private boolean isStraightFlush() {
		//Si es Flush y Straight, entonces es StraightFlush
		//No vuelvo a llamar el metodo isFlush() y isStraight(), porque verificando si es RoyalFlush asigne los valores
		//A las variables staticas isFlush y isStraight.
	    if (isFlush && isStraight) {
	        return true;
	    } else {
	        return false;
	    }
	}
	
	public boolean isFourOfAKind() {
		//Si existe una carta que se repite 4 veces, es un "Four of a Kind", adicionalmente asigno el valor de la carta
		//Con la que se consiguio esta mano, a la carta highFourKind para comparar en caso de que ambas manos tengan "For of a Kind"
	    int cardRepeats = 1;
	    int i = 0;
	    while (i < hand.length) {
	    	int k = i + 1;
	        cardRepeats = 1;
	        while (k < hand.length) {
	            if (hand[i].getRank().getValue() == hand[k].getRank().getValue()) {
	                cardRepeats++;
	                if (cardRepeats == 4) {
	                	highFourKind = hand[i];
	                    return true;
	                }
	            }
	            k++;
	        }
	        i++;
	    }
	    return false;
	}
	
	private boolean isFullHouse() {
		//Verifico si la mano tiene ThreeKind y OnePair (En este momento el resultado de isThreeOfAKind() y isOnePair()
		//Son asignados a las variables isThreeKind y isOnePair para no volver a llamar el metodo en caso de que se necesitase.
		isThreeKind = isThreeOfAKind();
		isOnePair = isOnePair();
		if (isThreeKind && isOnePair){
			return true;
		} else {
			return false;
		}
	}
	
	public boolean isFlush() {
		//Cuento las suits de la mano y devuelvo true en caso de contar 5 de algun tipo.
	    int countClubs = 0;
	    int countSpades = 0;
	    int countHearts = 0;
	    int countDiamonds = 0;
	    for (Card c : hand) {
	        switch (c.getSuit()) {
	            case "hearts":
	            	countHearts++;
	                break;
	            case "spades":
	            	countSpades++;
	                break;
	            case "clubs":
	            	countClubs++;
	                break;
	            case "diamonds":
	            	countDiamonds++;
	                break;
	        }
	    }
	    return (countClubs == 5 || countSpades == 5 || countHearts == 5 || countDiamonds == 5);
	}
	
	public boolean isStraight() {
		//Verifico que la resta del valor de cada carta de la mano (ordenada) sea de 1, 
		//o en algun caso de 9 para cuando es 2,3,4,5,A (La resta de A(14) - 5 = 9) 
	    int numConsecutive = 0;
	    int i = 0;
	    while (i < hand.length - 1) {
	        if (hand[i+1].getRank().getValue() - hand[i].getRank().getValue() == 1 || hand[i+1].getRank().getValue() - hand[i].getRank().getValue() == 9) {
	        	numConsecutive++;
	            i++;
	        } else {
	        	break;
	        }
	    }
	    if (numConsecutive == 4) {
            return true;
        } else {
        	return false;
        }
	}

	private boolean isThreeOfAKind() {
		//Verifico que exista una carta que se repita 3 veces.
		//De existir esa carta la almaceno en la variable highTreeKind.
	    int cardRepeats = 1;
	    int i = 0;
	    while (i < hand.length) {
	    	int k = i + 1;
	        cardRepeats = 1;
	        while (k < hand.length) {
	            if (hand[i].getRank().getValue() == hand[k].getRank().getValue()) {
	                cardRepeats++;
	                if (cardRepeats == 3) {
	                	highThreeKind = hand[i];
	                    return true;
	                }
	            }
	            k++;
	        }
	        i++;
	    }
	    return false;
	}

	private boolean isTwoPair() {
		//Verifico que exista 2 cartas que se repita 2 veces cada una.
		//En caso de encontrarlas, las guardo en highTwoPair1(De mayor valor) y highTwoPair2(De menor valor)
		//Para compararlas en caso de empate.
	    int cardRepeats = 1;
	    int noOfCardRepeats = 0;
	    Card pair1 = null;
	    Card pair2 = null;
	    int i = 0;
	    while (i < hand.length) {
	    	int k = i + 1;
	        cardRepeats = 1;
	        while (k < hand.length) {
	            if (hand[i].getRank().getValue() == hand[k].getRank().getValue()) {
	                cardRepeats++;
	                if (cardRepeats == 2) {
	                    noOfCardRepeats++;
	                    if (pair1 == null)
	                    	pair1 = hand[i];
	                    else pair2 = hand[i];
	                    if (noOfCardRepeats == 2) {
	                    	if (pair1.getRank().getValue() > pair2.getRank().getValue()) {
	                    		highTwoPair1 = pair1;
		                    	highTwoPair2 = pair2;
	                    	} else {
	                    		highTwoPair1 = pair2;
		                    	highTwoPair2 = pair1;
	                    	}
	                        return true;
	                    }
	                }

	            }
	            k++;
	        }
	        i++;
	    }
	    highTwoPair1 = null;
    	highTwoPair2 = null;
	    return false;
	}

	private boolean isOnePair() {
		//Determino si existe una carta que se repita 2 veces, en caso afirmativo
		//La almaceno en highOnePair para comparar en caso de empate.
	    int cardRepeats = 1;
	    int rankCardRepeat3 = 0;
	    Card pair = null;
	    boolean isPair = false;
	    int i = 0;
	    while (i < hand.length && !isPair) {
	    	int k = i + 1;
	        cardRepeats = 1;
	        while (k < hand.length && !isPair) {
	            if (hand[i].getRank().getValue() == hand[k].getRank().getValue() && hand[i].getRank().getValue() != rankCardRepeat3) {
	                cardRepeats++;
	                if (cardRepeats == 2) {
	                	pair = hand[i];
	                }
	                if (cardRepeats == 3) {
	                	rankCardRepeat3 = hand[i].getRank().getValue(); 
	                	pair = null;
	    	        }
	            }
	            k++;
	        }
	        i++;
	        if (cardRepeats == 2) {
                isPair = true;
                highOnePair = pair;
            }
	    }
	    return isPair;
	}

	//Como al crearse este objeto se ordena la mano de menor a mayor
	//Este metodo me devuelve la carta que necesite
	//Siendo getHighCard(4) la mayor carta, y getHighCard(0) la menor carta.
	public Card getHighCard(int pos) {
	    return hand[pos];
	}
}
