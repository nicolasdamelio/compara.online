package comparaonline.poker.controlador;

import java.io.IOException;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.VariableResolver;

/**
 * CGeneric
 * 
 * @author Nicolás D'Amelio
 * @version 1.0
 */
@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public abstract class CGeneric extends SelectorComposer<Component> {

	private static final long serialVersionUID = -2264423023637489596L;

	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		inicializar();
	}

	public abstract void inicializar() throws IOException;

}
