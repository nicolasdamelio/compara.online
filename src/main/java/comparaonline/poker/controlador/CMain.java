package comparaonline.poker.controlador;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import javax.net.ssl.HttpsURLConnection;

import org.json.JSONArray;
import org.json.JSONObject;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;

import comparaonline.poker.modelo.Card;
import comparaonline.poker.modelo.Hand;
import comparaonline.poker.modelo.Hand.HandRank;
import comparaonline.poker.modelo.Rank;

/**
 * CMain
 * 
 * @author Nicol�s D'Amelio
 * @version 1.0
 */
public class CMain extends CGeneric {

	private static final long serialVersionUID = 1L;

	@Wire
	private Label h1_c1;
	@Wire
	private Label h1_c2;
	@Wire
	private Label h1_c3;
	@Wire
	private Label h1_c4;
	@Wire
	private Label h1_c5;
	@Wire
	private Label h2_c1;
	@Wire
	private Label h2_c2;
	@Wire
	private Label h2_c3;
	@Wire
	private Label h2_c4;
	@Wire
	private Label h2_c5;
	@Wire
	private Label lblResult1;
	@Wire
	private Label lblResult2;
	@Wire
	private Label lblDeck;
	@Wire
	private Hbox winner1;
	@Wire
	private Hbox winner2;
	@Wire
	private Hbox tie1;
	@Wire
	private Hbox tie2;
	@Wire
	private Button btnDealsCards;
	
	private String myDeck;

	public void inicializar() throws IOException {
		//Reviso que no exita un deck creado en session.
		myDeck = (String) Sessions.getCurrent().getAttribute("tokenDeck");
	}

	//Solicito un nuevo Deck al servicio proporcionado por comparaonline.
	@Listen("onClick = #btnNewDeck")
	public void shuffleDeck() {
		URL obj;
		String result = null;
		int responseCode = 0;
		int attempts = 0; 
		do {
			try {
				obj = new URL("https://services.comparaonline.com/dealer/deck");
				URLConnection con = obj.openConnection();

				((HttpsURLConnection) con).setRequestMethod("POST");

				responseCode = ((HttpURLConnection) con).getResponseCode();

				if (responseCode != 200) {
					System.out.println(responseCode);
					attempts++;
				} else {
					BufferedReader in = new BufferedReader(
							new InputStreamReader((con.getInputStream())));
					String inputLine;
					StringBuffer response = new StringBuffer();

					while ((inputLine = in.readLine()) != null) {
						response.append(inputLine);
					}
					in.close();

					result = response.toString();
				}

			} catch (MalformedURLException e1) {
				e1.printStackTrace();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		} while (responseCode != 200 && attempts<10); 
		//Hasta que no retorne un codigo de exito sigo intentando 
		//(esto es para evitar esos errores generados por el dealer service).
		//O hasta que el numero de intentos de solicitud de deck sea igual a 10,
		//esto para evitar un posible infinite loop.
		
		if (responseCode != 200) {
			Messagebox.show("Some problems in the dealer service prevented shuffling a new deck, please try later",
					"Error", Messagebox.OK, Messagebox.INFORMATION);
		} else {
			Sessions.getCurrent().setAttribute("tokenDeck", result);
			myDeck = result;
			btnDealsCards.setDisabled(false);
			System.out.println(result);
			lblDeck.setVisible(true);
			lblDeck.setValue("Deck " + result);
			lblDeck.setStyle("font-style: italic; color: gray; font-size: larger;");
		}
	}

	//Al solicitar nuevas cartas invoco este metodo.
	@Listen("onClick = #btnDealsCards")
	public void dealsCards() {
		//Llamo al servicio que retorna las cartas.
		String cartas = dealsHand();
		if (cartas.equals("404")) {
			btnDealsCards.setDisabled(true);
			lblDeck.setVisible(false);
			Messagebox.show("The deck has expired, please shuffle another",
					"Error", Messagebox.OK, Messagebox.INFORMATION);
		} else if (cartas.equals("405")) {
			btnDealsCards.setDisabled(true);
			lblDeck.setVisible(false);
			Messagebox
					.show("There aren�t enough cards to deal, please shuffle a new deck",
							"Error", Messagebox.OK, Messagebox.INFORMATION);
		} else if (cartas.equals(null)) {
			btnDealsCards.setDisabled(true);
			lblDeck.setVisible(false);
			Messagebox.show("First shuffle a new deck", "Error", Messagebox.OK,
					Messagebox.INFORMATION);
		} else {
			//Creo un JSON con las cartas
			JSONObject jsonObj = new JSONObject("{\"hands\":" + cartas + "}");
			//Obtengo el JSON en forma de Array.
			JSONArray jsonArray = jsonObj.getJSONArray("hands");

			//Inicializo las cartas por cada mano (1 y 2).
			Card[] cards1 = new Card[5];
			Card[] cards2 = new Card[5];

			//Asigno las cartas a cada mano (0-4 Mano 1) (5-9 Mano 2)
			for (int i = 0; i < 5; i++) {
				String number1 = jsonArray.getJSONObject(i).getString("number");
				String suit1 = jsonArray.getJSONObject(i).getString("suit");
				String number2 = jsonArray.getJSONObject(i + 5).getString("number");
				String suit2 = jsonArray.getJSONObject(i + 5).getString("suit");
				int rankCard1 = 0;
				int rankCard2 = 0;
				//Este switch case es para asignarle un valor numerico a cada carta
				//J Q K A => 11 12 13 y 14 respectivamente.
				switch (number1) {
					case "A":
						rankCard1 = 14;
						break;
					case "2":
						rankCard1 = 2;
						break;
					case "3":
						rankCard1 = 3;
						break;
					case "4":
						rankCard1 = 4;
						break;
					case "5":
						rankCard1 = 5;
						break;
					case "6":
						rankCard1 = 6;
						break;
					case "7":
						rankCard1 = 7;
						break;
					case "8":
						rankCard1 = 8;
						break;
					case "9":
						rankCard1 = 9;
						break;
					case "10":
						rankCard1 = 10;
						break;
					case "J":
						rankCard1 = 11;
						break;
					case "Q":
						rankCard1 = 12;
						break;
					case "K":
						rankCard1 = 13;
						break;
				}

				switch (number2) {
					case "A":
						rankCard2 = 14;
						break;
					case "2":
						rankCard2 = 2;
						break;
					case "3":
						rankCard2 = 3;
						break;
					case "4":
						rankCard2 = 4;
						break;
					case "5":
						rankCard2 = 5;
						break;
					case "6":
						rankCard2 = 6;
						break;
					case "7":
						rankCard2 = 7;
						break;
					case "8":
						rankCard2 = 8;
						break;
					case "9":
						rankCard2 = 9;
						break;
					case "10":
						rankCard2 = 10;
						break;
					case "J":
						rankCard2 = 11;
						break;
					case "Q":
						rankCard2 = 12;
						break;
					case "K":
						rankCard2 = 13;
						break;
				}

				//Creo los objetos Rank, con ellos creo los objetos Card. Luego agrega las Cards a un array Card[5]
				Rank r1 = new Rank(rankCard1, number1);
				Rank r2 = new Rank(rankCard2, number2);
				Card c1 = new Card(r1, suit1);
				Card c2 = new Card(r2, suit2);
				cards1[i] = c1;
				cards2[i] = c2;
			}
			//Finalmente con el array Card[5] creo el objeto Hand.
			Hand h1 = new Hand(cards1);
			Hand h2 = new Hand(cards2);

			//Aqui envio las cartas a la pantalla.
			h1_c1.setValue("1. " + cards1[0].getRank().getRank() + " " + cards1[0].getSuit());
			h1_c2.setValue("2. " + cards1[1].getRank().getRank() + " " + cards1[1].getSuit());
			h1_c3.setValue("3. " + cards1[2].getRank().getRank() + " " + cards1[2].getSuit());
			h1_c4.setValue("4. " + cards1[3].getRank().getRank() + " " + cards1[3].getSuit());
			h1_c5.setValue("5. " + cards1[4].getRank().getRank() + " " + cards1[4].getSuit());

			h2_c1.setValue("1. " + cards2[0].getRank().getRank() + " " + cards2[0].getSuit());
			h2_c2.setValue("2. " + cards2[1].getRank().getRank() + " " + cards2[1].getSuit());
			h2_c3.setValue("3. " + cards2[2].getRank().getRank() + " " + cards2[2].getSuit());
			h2_c4.setValue("4. " + cards2[3].getRank().getRank() + " " + cards2[3].getSuit());
			h2_c5.setValue("5. " + cards2[4].getRank().getRank() + " " + cards2[4].getSuit());

			//Llamo el metodo que realiza la comparativa entre ambas manos.
			resultPoker(h1, h2);
		}

	}

	//Aqui se determina la mano ganadora.
	public void resultPoker(Hand h1, Hand h2) {
		//Hand.determineHandRank() retorna un Enum HandRank con el nombre del Hand Ranking (HighCard .... RoyalFlush)
		//y un puntaje respectivo de la mano desde HighCard = 1 hasta RoyalFlush = 10.
		HandRank resultHand1 = h1.determineHandRank();
		HandRank resultHand2 = h2.determineHandRank();

		//Escribo el nombre del HandRanking en pantalla por cada mano.
		lblResult1.setValue("Result: " + resultHand1.name().replace("_", " "));
		lblResult2.setValue("Result: " + resultHand2.name().replace("_", " "));

		//Comparo los puntajes de las manos para saber cual es la ganadora
		if (resultHand1.points() > resultHand2.points()) {
			showWinner(1);
		} else if (resultHand1.points() < resultHand2.points()) {
			showWinner(2);
		} else {
			//En caso de empate me voy a un switchCase para determinar el desempate en cada caso respectivo de mano que se tenga.
			//Esto porque es distinto determinar el empate en cada caso.
			int i = 0;
			boolean findIt1 = false;
			switch (resultHand1.points()) {
				case 10:
					//Royal_Flush
					//En el extremo caso de que ambas manos tengan RoyalFlush es un empate.
					showWinner(0);
					break;
				case 9:
					//Straight_Flush
					//En este caso, basta con determinar el highCard para determinar el ganador
					//En caso de que ambas tengan la misma highCard es empate.
					if (h1.getHighCard(4).getRank().getValue() > h2.getHighCard(4).getRank().getValue()) {
						showWinner(1);
					} else if (h1.getHighCard(4).getRank().getValue() < h2.getHighCard(4).getRank().getValue()) {
						showWinner(2);
					} else {
						showWinner(0);
					}
					break;
				case 8:
					//Four_of_a_Kind
					//En este caso consulto la carta con la que se obtuvo el "Four of a Kind" en ambas manos y las comparo.
					//Obviamente aca no puede existir empate ya que solo existen 4 cartas de cada valor.
					if (h1.getHighFourKind().getRank().getValue() > h2.getHighFourKind().getRank().getValue()) {
						showWinner(1);
					} else  {
						showWinner(2);
					} 
					break;
				case 7:
					//Full_House
					//En este caso comparo las cartas con las que se obtuvieron ThreeKind.
					//Obviamente no puede existir empate ya que solo existen 4 cartas de cada valor.
					if (h1.getHighThreeKind().getRank().getValue() > h2.getHighThreeKind().getRank().getValue()) {
						showWinner(1);
					} else {
						showWinner(2);
					} 
					break;
				case 6:
					//Flush
					//En este caso comparo las highCards de ambas manos, en caso de ser iguales paso a la siguiente highCard
					//Y asi sucesivamente hasta encontrar un ganador o si llegase a consultar todas las cartas y son iguales
					//Seria un empate.
					findIt1 = false;
					i = 4;
					while (!findIt1 && (i>=0)){
						if (h1.getHighCard(i).getRank().getValue() > h2.getHighCard(i).getRank().getValue()){
							findIt1 = true;
							showWinner(1);
						} else if (h1.getHighCard(i).getRank().getValue() < h2.getHighCard(i).getRank().getValue()) {
							findIt1 = true;
							showWinner(2);
						} else
							i--;
					}
					if (!findIt1)
						showWinner(0);
					break;
				case 5:
					//Straight
					//Este es el mismo caso de StraightFlush, consulto la highCard de ambas manos para determinar
					//la mano ganadora, y en caso de ser iguales es un empate.
					if (h1.getHighCard(4).getRank().getValue() > h2.getHighCard(4).getRank().getValue()) {
						showWinner(1);
					} else if (h1.getHighCard(4).getRank().getValue() < h2.getHighCard(4).getRank().getValue()) {
						showWinner(2);
					} else {
						showWinner(0);
					}
					break;
				case 4:
					//Three_of_a_Kind
					//Comparo las cartas con las que se obtuvo el "Three of a Kind" y determino el ganador,
					//Como solo existen 3 por tipo, no puede haber empate.
					if (h1.getHighThreeKind().getRank().getValue() > h2.getHighThreeKind().getRank().getValue()) {
						showWinner(1);
					} else {
						showWinner(2);
					}
					break;
				case 3:
					//Two_Pair
					//Comparo el valor del par mayor de ambas manos, en caso de ser iguales comparo el valor del
					//siguiente par, en caso de ser iguales comparo el valor de la ultima carta restante,
					//y en caso de ser igual es un empate.
					if (h1.getHighTwoPair1().getRank().getValue() > h2.getHighTwoPair1().getRank().getValue()) {
						showWinner(1);
					} else if (h1.getHighTwoPair1().getRank().getValue() < h2.getHighTwoPair1().getRank().getValue()) {
						showWinner(2);
					} else if (h1.getHighTwoPair2().getRank().getValue() > h2.getHighTwoPair2().getRank().getValue()) {
						showWinner(1);
					} else if (h1.getHighTwoPair2().getRank().getValue() < h2.getHighTwoPair2().getRank().getValue()) {
						showWinner(2);
					} else {
						i = 4;
						int n = 0;
						int m = 0;
						
						for (int j = 4; j >= 0; j--) {
							if (h1.getHighCard(j).getRank().getValue() != h1.getHighTwoPair1().getRank().getValue() && h1.getHighCard(j).getRank().getValue() != h1.getHighTwoPair2().getRank().getValue()){
								n = j;
							}
							if (h2.getHighCard(j).getRank().getValue() != h2.getHighTwoPair1().getRank().getValue() && h2.getHighCard(j).getRank().getValue() != h2.getHighTwoPair2().getRank().getValue()){
								m = j;
							}
						}
						if (h1.getHighCard(n).getRank().getValue() > h2.getHighCard(m).getRank().getValue()){
							showWinner(1);
						} else if (h1.getHighCard(n).getRank().getValue() < h2.getHighCard(m).getRank().getValue()){
							showWinner(2);
						} else {
							showWinner(0);
						}	
					}
					break;
				case 2:
					//One_Pair
					//Comparo el valor de la carta con la que se obtuvo el par, y en caso de ser iguales
					//Comparo las 3 cartas restantes de la mano en orden descedente para determinar el ganador
					//y en caso de ser iguales es un emmpate.
					if (h1.getHighOnePair().getRank().getValue() > h2.getHighOnePair().getRank().getValue()) {
						showWinner(1);
					} else if (h1.getHighOnePair().getRank().getValue() < h2.getHighOnePair().getRank().getValue()) {
						showWinner(2);
					} else {
						i = 4;
						int n = 0;
						Card[] c1 = new Card[3];
						int m = 0;
						Card[] c2 = new Card[3];
						
						for (int j = 4; j >= 0; j--) {
							if (h1.getHighCard(j).getRank().getValue() != h1.getHighOnePair().getRank().getValue()){
								c1[n] = h1.getHighCard(j);
								n++;
							}
							if (h2.getHighCard(j).getRank().getValue() != h2.getHighOnePair().getRank().getValue()){
								c2[m] = h2.getHighCard(j);
								m++;
							}
						}
						if (c1[0].getRank().getValue() > c2[0].getRank().getValue()){
							showWinner(1);
						} else if (c1[0].getRank().getValue() < c2[0].getRank().getValue()){
							showWinner(2);
						} else if (c1[1].getRank().getValue() > c2[1].getRank().getValue()){
							showWinner(1);
						} else if (c1[1].getRank().getValue() < c2[1].getRank().getValue()){
							showWinner(2);
						} else if (c1[2].getRank().getValue() > c2[2].getRank().getValue()){
							showWinner(1);
						} else if (c1[2].getRank().getValue() < c2[2].getRank().getValue()){
							showWinner(2);
						} else {
							showWinner(0);
						}
					}
					break;
				case 1:
					//High_Card
					//Al igual que Flush, comparo highCard de ambas manos, y en caso de ser iguales voy descendendiendo hasta
					//encontrar una carta mayor a la otra, o hasta terminar la mano y determinar un empate.
					findIt1 = false;
					i = 4;
					while (!findIt1 && (i>=0)){
						if (h1.getHighCard(i).getRank().getValue() > h2.getHighCard(i).getRank().getValue()){
							findIt1 = true;
							showWinner(1);
						} else if (h1.getHighCard(i).getRank().getValue() < h2.getHighCard(i).getRank().getValue()) {
							findIt1 = true;
							showWinner(2);
						} else
							i--;
					}
					if (!findIt1)
						showWinner(0);
					break;
			}
		}

	}

	//Metodo que invoca el dealer service proporcionado por comparaonline para la entrega de # cartas.
	public String dealsHand() {
		int attempts = 0;
		String result = null;
		// Si existe un deck proporcionado
		if (myDeck != null) {
			URL obj;
			int responseCode = 0;
			do {
				try {
					//Solicito 10 cartas y luego asigno Hand1 = Cartas[0-4] y Hand2 = Cartas[5-9]
					obj = new URL("https://services.comparaonline.com/dealer/deck/"+ myDeck + "/deal/10");
					HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();

					responseCode = ((HttpsURLConnection) con).getResponseCode();

					if (responseCode == 405) {
						//retorno codigo 405
						result = String.valueOf(responseCode);
						System.out.println(responseCode);
					} else if (responseCode == 404) {
						//retorno codigo 404
						result = String.valueOf(responseCode);
						System.out.println(responseCode);
					} else if (responseCode == 200) {
						BufferedReader in = new BufferedReader(
								new InputStreamReader((con.getInputStream())));
						String inputLine;
						StringBuffer response = new StringBuffer();

						while ((inputLine = in.readLine()) != null) {
							response.append(inputLine);
						}
						in.close();
						//retorno cartas
						result = response.toString();
					} else {
						attempts++;
					}
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			} while (responseCode != 200 && responseCode != 405 && responseCode != 404 && attempts<10);
			// Ejecuto este ciclo hasta retornar los posibles codigos que son:
			// 404: Deck Expiro, 405: Insuficientes Cartas para repartir, 200: Exito.
			// O hasta que el numero de intentos de solicitud de deck sea igual a 10,
			// esto para evitar un posible infinite loop.
			System.out.println(result);

		}
		return result;
	}
	
	//Un metodo para dibujar en pantalla el ganador de la mano o en caso extremo un empate.
	public void showWinner(int who){
		//0 = tie, 1 = handOne, 2 = handTwo
		switch (who) {
			case 0:
				winner1.setVisible(false);
				winner2.setVisible(false);
				tie1.setVisible(true);
				tie2.setVisible(true);
				break;
			case 1:
				winner1.setVisible(true);
				winner2.setVisible(false);
				tie1.setVisible(false);
				tie2.setVisible(false);
				break;
			case 2:
				winner1.setVisible(false);
				winner2.setVisible(true);
				tie1.setVisible(false);
				tie2.setVisible(false);
				break;
		}
	}
}
