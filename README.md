# INSTRUCCIONES #

Descargar el servidor de aplicaciones glassfish 4.0 del siguiente link:
http://download.oracle.com/glassfish/4.0/release/glassfish-4.0-windows.exe
*Update: deje el instalador de glassfish4, para windows en la carpeta doc.*

Para instalar el servidor glassfish previamente se debe instalar el jdk de java, en este caso utilice java ee 7,
puede descargarse del siguiente link:
http://www.oracle.com/technetwork/java/javase/downloads/java-archive-downloads-javase7-521261.html
*Update: deje el instalador de jdk7, para windows x64 en la carpeta doc.*

Deberia funcionar con cualquier version de java 7, en mi caso tengo instalado la siguiente version: 
Java SE Development Kit 7u79 x64
Agregar en variables de entorno, 
JAVA_HOME = C:\Program Files\Java\jdk1.7.0_79  <-- Depende del directorio donde instale java.

Ya con esto deberia poder instalarse el glassfish.
Una vez instalado, en el bash, dirigirse a la carpeta de instalacion de glassfish
Por defecto es C:\glassfish4, una vez alli dirigirse a C:\glassfish4\glassfish\bin y ejectuar el comando "asadmin"
C:\glassfish4\glassfish\bin> asadmin
asadmin> start-domain domain1 (<--- Luego ejecutar este comando)

Y eso deberia levantar el servidor,

* Dirigirse al navegador a localhost:4848
* Dirigirse al apartado "Applications", click alli.
* Dar click en la opcion deploy, alli seleccionar el archivo poker.war que deje en la carpeta doc del proyecto.
* Dejar todo por defecto, y pulsar en "OK", en la esquina superior derecha.
* Una vez finalizado el deploy, se devolvera a la pantalla anterior, alli mismo puede dar click en "Launch" y escoger cualquiera de las dos opciones, o ir directamente al navegador y navegar hacia http://localhost:8080/poker


La estructura del proyecto como puede observarse en GIT,

+ .settings
+ doc (glassfish, jdk y war senalados anteriormente)
	- glassfish-4.0-windows.exe
	- jdk-7u79-windows-x64.exe
	- poker.war
+ src/main
	+ assembly
	+ java/comparaonline/poker
		+ controlador
			- CGeneric.java (Controlador del que hereda CMain donde se relaciona con el framework ZK)
			- CMain.java (Controlador principal del proyecto)
		+ modelo
			- Card.java
			- Hand.java (Aqui estan los metodos que determinan que mano se tiene)
			- Rank.java
	+ resources/META-INF
		- ConfiguracionAplicacion.xml
		- persistence.xml
	+ webapp
		+ META-INF
			- MANIFEST.MF
		+ WEB-INF
			- applicationContext.xml
			- glassfish-web.xml
			- web.servlet-3.xml
			- web.xml
			- zk.xml
		+ images (Imagenes utilizadas en la vista)
		- index.zul (Esta es la interfaz del aplicativo web).